Empowerments

---
title: "ROMAIN DI VOZZO"
description: "Romain Di Vozzo's CV"
cascade:
featured_image: '/static/romain_portrait_2022.jpg'
---
![](https://gitlab.com/romaindivozzo/empowerments/-/raw/master/static/romain_portrait_2022.jpg)
<!--![](https://gitlab.com/fablabdigiscope/fablabdigiscope.gitlab.io/-/raw/master/static/images/team/romain-di-vozzo.jpg)-->

<!--<img src="drawing.jpg" alt="drawing" style="width:200px;"/>-->

&nbsp;
&nbsp;

Hello, this is where I list what I do.

I founded [FABLAB UPSACLAY](https://fablabdigiscope.universite-paris-saclay.fr/), the french `Super Node` of the globally distributed fablab network. I am its director. FABLAB UPSaclay is a branch of the Laboratoire Interdisciplinaire des Sciences du Numérique (LISN - Université Paris-Saclay - UMR 9015 - CNRS).

I am active in fields that generally bearly overlap in Academia like Design, Digital Fabrication, Political Sciences, Science and technology studies, Activism, Distributed and STEAM Education. I am also mesmerized by Synthetic Biology...

Check my [LinkedIn page](https://www.linkedin.com/in/romaindivozzo/) to see my posts and read the cv below to get a better understanding of my activities and where they extend.

&nbsp;
&nbsp;

## `EDUCATION`

| `DIPLOMA -->` | `FAB ACADEMY` |
| :- | :- |
| ` WEBSITE` | [https://fabacademy.org/](https://fabacademy.org/) |
| `YEAR :` | 2012 |
| `FIELD :` | Design & Digital Fabrication & Distributed Education |
| `INSTITUTION :` | AS220 FABLAB + Center for Bits and Atoms |
| `MENTORS :` | Pr. Neil Gershenfeld (MIT) + Anna Kaziunas France |
| `DOCUMENTATION :` | [http://fabacademy.org/archives/2012/students/di_vozzo.romain/index.html](http://fabacademy.org/archives/2012/students/di_vozzo.romain/index.html)
| Please scroll all the way down to see the list of Fab Academy classes | - |

&nbsp;
&nbsp;

| ![](https://gitlab.com/romaindivozzo/empowerments/-/raw/master/static/ROM_FAB-ACADEMY-2012.jpg) |
| - |

&nbsp;
&nbsp;

| `DIPLOMA -->` | `MASTER SPEAP` |
| :- | :- |
| ` WEBSITE` | [https://www.sciencespo.fr/public/fr/formations/master-arts-politics.html](https://www.sciencespo.fr/public/fr/formations/master-arts-politics.html) |
| `YEAR :` | 2012-13 |
| `FIELD :` | Arts & Politics |
| `INSTITUTION :` | AS220 FABLAB + Center for Bits and Atoms |
| `MENTORS :` | Pr. Bruno Latour + Valérie Pihet |
| Please scroll all the way down to see the list of SPEAP seminars | - |

&nbsp;
&nbsp;

| ![](https://gitlab.com/romaindivozzo/empowerments/-/raw/master/static/ROM_SPEAP_2013_2.jpg) |
| - |



&nbsp;
&nbsp;

| `DIPLOMA -->` | `BIO ACADEMY` |
| :- | :- |
| ` WEBSITE` | [http://bio.academany.org/index.html](http://bio.academany.org/index.html) |
| `YEAR :` | 2015-16 |
| `FIELD :` | Bio Design & Synthetic Biology & Distributed Education |
| `INSTITUTION :` | Fablab UPSaclay + Harvard WYSS Institute + CBA/MIT |
| `MENTORS :` | Pr George Church (Harvard) + David Sun Kong (MIT) |
| Please scroll all the way down to see the list of Bio Academy classes | - |

&nbsp;
&nbsp;

| ![](https://gitlab.com/romaindivozzo/empowerments/-/raw/master/static/ROM_BIO-ACADEMY-2016.png) |
| - |

&nbsp;
&nbsp;

| `DIPLOMA -->` | `FABRICADEMY` |
| :- | :- |
| `YEAR :` | 2018-19 |
| `FIELD :` | Fashion Design & Digital Fabrication & Distributed Education |
| `INSTITUTION :` | Fablab UPSaclay + Institute for Advanced Architecture Cataluña |
| `MENTORS :` | Anastasia Pistofidou + Cecilia Raspanti |
| `DOCUMENTATION :` | [https://class.textile-academy.org/2019/students/romaindivozzo/](https://class.textile-academy.org/2019/students/romaindivozzo/) |
| Please scroll all the way down to see the list of Fabricademy classes | - |

&nbsp;
&nbsp;

| ![](https://gitlab.com/romaindivozzo/empowerments/-/raw/master/static/ROM_FABRICADEMY-2019_2.jpg) |
| - |

&nbsp;

| ![](http://class.textile-academy.org/2019/romaindivozzo/images/ro_fabrica-final-project-pix/ro_poster_fabricademy_final-project_hires.jpg) |
| - |

&nbsp;

| ![](http://class.textile-academy.org/2019/romaindivozzo/images/ro_fabrica-final-project-pix/ro_phitolacca.jpg) |
| - |

&nbsp;

{{< youtube -0pWKrBwssk>}}


<iframe width="560" height="315" src="https://www.youtube.com/embed/-0pWKrBwssk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

&nbsp;
&nbsp;

## `THE DIPLOMA I HAVE CREATED`
| `NAME :` | "DIPLÔME UNIVERSITAIRE FAB ACADEMY+` |
| :- | :- |
| `YEAR :` | Since 2019 |
| `FIELD :` | Design & Digital Fabrication & Human-Machine Interaction |
| `INSTITUTION :` | Université Paris-Saclay + Fab Foundation + Pr Neil Gershenfeld (CBA/MIT) |
| `MENTORS (UPSaclay) :` | Romain Di Vozzo + Pr Michel Beaudouin Lafon |
| `MENTORS (Fab Foundation) :` | Romain Di Vozzo + Pr Neil Gershenfeld | | `WEBPAGE :` | [https://www.universite-paris-saclay.fr/du-fab-academy-upsaclay](https://www.universite-paris-saclay.fr/du-fab-academy-upsaclay)

&nbsp;
&nbsp;

## `COMMUNITIES I AM A PART OF :`

| The Global Fablab Network : I worked/collaborate to this community in many ways since 2012, like being the Global Coordinator for the [FAB14](http://fab14.fabevent.org/#speakers) Conference, the [Global Fablab Network Annual Gathering]. I am also part of the [Fab Foundation Deployment Team](https://fabfoundation.org/deployment/) |
| - |



&nbsp;
&nbsp;

![](https://gitlab.com/romaindivozzo/empowerments/-/raw/master/static/ROM_FAB14_2018.jpg)
| - |

&nbsp;
&nbsp;

| I am also part of the [Global Community Bio Summit](https://archive.biosummit.org/romain-di-vozzo), a community event [I join each year](https://www.biosummit.org/) since 2017 |
| - |
| ![](https://gitlab.com/romaindivozzo/empowerments/-/raw/master/static/ROM_GLOBAL-BIO-SUMMIT-2018.jpg)


&nbsp;
&nbsp;

| EUGLOH (European Alliance for Global Health) : This year I curated, organized and hosted the `EUGLOH Innovation Days` on the topic [`Systemic Design for the Post-Oil Era`](https://www.eugloh.eu/events/eugloh-innovation-days-2022). And you can check this posts to know more about it : [1](https://www.linkedin.com/posts/romaindivozzo_innovationdays-empowerment-commons-activity-6947974160375541760-GVXp?utm_source=share&utm_medium=member_desktop), [2](https://www.linkedin.com/posts/romaindivozzo_jamesauger-activity-6948670200200220672-NNdy?utm_source=share&utm_medium=member_desktop), [3](https://www.linkedin.com/posts/romaindivozzo_eugloh-systemic-design-for-the-post-oil-activity-6949476267633078272-p9EJ?utm_source=share&utm_medium=member_desktop). |
| - |
| ![](https://gitlab.com/romaindivozzo/empowerments/-/raw/master/static/ROMA_EUGLOH-INN-DAYS_2022.jpeg) |

&nbsp;
&nbsp;

| Also check the [CREARTATHON'S WEBPAGE](https://creartathon.com/persons) |
| - |
| ![k](https://pbs.twimg.com/media/E52rGOPVIAUO6S5?format=jpg&name=medium) |


&nbsp;
&nbsp;

| I have also created/curated a new event named [EMPOWER UNIVERSITÉ PARIS-SACLAY](https://www.linkedin.com/showcase/empower-upsaclay-2024/?viewAsMember=true) |
| - |
| ![](https://empower-upsaclay-2024-romaindivozzo-b77b9139353256b18c2c2d32843.gitlab.io/images/ROM_EMPOWERUPSACLAY24_POSTER_3SEPT.jpeg) |

-e

&nbsp;
&nbsp;

## `WEBSITES AND WEB PAGES I RUN`

| FABLAB UPSACLAY | https://fablabdigiscope.universite-paris-saclay.fr/ (currently down because of the cyberattack)|
| - | -: |
| FAB ACADEMY FRANCE | https://fabacademy.fr/ |
| TEACHING PAGE | https://romaindivozzo.gitlab.io/teaching/ |
| SENSOR TUTORIALS (Work in progress) | https://romaindivozzo.gitlab.io/sensors/ |
| FABLAB UPSACLAY STUDENTS BOOK (In process) | https://fablabdigiscope.gitlab.io/upsaclay-students-handbook/ |
| EMPOWER UNIVERSITÉ PARIS-SACLAY | https://romaindivozzo.gitlab.io/empower-upsaclay-2024/ |

&nbsp;
&nbsp;

| `LANGUAGES` | French (native) | English (fluent) | Spanish (fluent) |
| - | - | - | - |

&nbsp;
&nbsp;

| Current Activities in 2024 |
| :- |
| FABLAB UPSACLAY Director (since 2013) |
| FABLAB UPSACLAY NETWORK Project Manager (since 2018) |

&nbsp;
&nbsp;

| Former Activities |
| :- |
| Research and Development Engineer (Level 2), AVIZ Team + Ex Situ Team, INRIA |
| Fablab Governance Board-Member (Fab Foundation) |
| Fab Economy Crew Member (Fab Foundation) |
| Fab Academy Supply chain Communications and Quotes (Fab Foundation) |
| Fablab Manager | Fablab Digiscope UPSaclay |
| Fablab Super Node (Fab Foundation) |
| Fab Academy Instructor (Fab Foundation) |
| Fab Academy Global Evaluation Reviewer (Fab Foundation) |
| Bio Academy Instructor (Fab Foundation) |
| Bio Academy Staff (Fab Foundation) |
| Fabricademy Instructor (Fab Foundation) |
| Consultant |
| Local Community Support (Ultimaker) |
| FAB14 Global Coordinator |

&nbsp;
&nbsp;

| `COMPUTING` |
| - |
| Version Control System : GIT + GITLAB |
| Web : HUGO, MKDOCS, GITBOOKS |
| 3D modeling : FUSION 360 |
| 3D printing : CURA, PRUSA SLICER, PREFORM  |
| 2D design : FUSION 360, COREL DRAW |
| Video editing : KDENLIVE |
| Electronics : ARDUINO IDE (basics) + sensors (light, pression, co2, temperature, etc.) |
| Electronics : ESP 32 and ESP 8266 (basics) + sensors (light, pression, co2, temperature, etc.) |
| Experimented with : caDNAno, Rosetta Commons, CobraPy, etc |

&nbsp;
&nbsp;

| `DIGITAL FABRICATION MACHINERY I RUN :` |
| - |
| EPILOG Laser Cutters (Fusion Fiber + CO2 or Mini) |
| ULTIMAKER : UMO, UM2+, UM3, UMS5 (FFF 3D Printers) |
| PRUSA i3 MK3S (FFF 3D Printers) |
| FORM2 (SLA 3D Printers) |
| ROLAND CAM1 GS24 (Vinyl Cutter) |
| ROLAND MODELA MDX40-A (Milling Machine) |
| SHOPBOT DESKTOP (D2418) (Milling Machine) |
| SHAPEOKO XXL (Milling Machine) |
| SHAPER ORIGIN (semi-automatic milling-machine)|

&nbsp;
&nbsp;
## `EXPERIENCE`

| `2024 (France)` |
| - |
| Art Production. Pierre Huyghe Studio. MoMA (USA) + LEEUM Museum (South Korea) |
| Fair. Creation, curation, coordination of EMPOWER UNIVERSITÉ PARIS-SACLAY, Maker-Faire-like event by UPSaclay. Centrale-Supélec. Gif-sur-yvette, France. |
| Talk + Workshop. Innovation between eugloh partners. EUGLOH. Fablab UPSaclay. Gif-sur-Yvette, France. |
| Round Table + Workshop. Faire Festival. ROSA LAB, Toulouse, France. |
| Seminar. "Introduction to 3D Printing". EUGLOH. Szeged University, LAMELIS (summer school). Gif-sur-Yvette, France. |
| Mentoring/Teaching. Master ARRC - Arts-Sciences-Technologies-social sciences (ENS Paris-Saclay). Fablab UPSaclay. Gif-sur-Yvette, France. |
| Mentoring/Teaching : Digital Fabrication Course. HCI/IOT-UPSaclay Master Human Computer Interaction (M1 and M2). Fablab UPSaclay. Gif-sur-Yvette, France. |
| Mentoring/Teaching : Studio Art Course. HCI/IOT-UPSaclay Master Human Computer Interaction (M1 and M2). Fablab UPSaclay. Gif-sur-Yvette, France. |
| Mentoring. "TP Innovant FAUCON" (Master Level) with Graduate School of Chemistry and Polytech. Fablab UPSaclay. Gif-sur-Yvette, France. |
| Seminar. Licence 2 Droit, Sciences et Innovation. 3D Printing Principles. Fablab UPSaclay. Gif-sur-Yvette, France. |
| Work (continuous position/11th year). Director and Research engineer/ project Manager. Fablab UPSaclay + Université Paris Saclay. Gif-sur- Yvette, France. |

&nbsp;
&nbsp;
&nbsp;
&nbsp;

| `2023 (France)` |
| - |
| Mentoring. Master M2R - Design Research (ENS Paris-Saclay). Fablab UPSaclay. Gif-sur-Yvette, France. |
| Mentoring/Teaching. Master ARRC - Arts-Sciences-Technologies-social sciences (ENS Paris-Saclay). Fablab UPSaclay. Gif-sur-Yvette, France. |
| Mentoring. EIT-UPSaclay Master Human Computer Interaction (HCI/HCID M1 and M2). Fablab UPSaclay. Gif-sur-Yvette, France. |
| Mentoring. "TP Innovant FAUCON" (Master Level) with Graduate School of Chemistry and Polytech. Fablab UPSaclay. Gif-sur-Yvette, France. |
| Teaching. LICENCE 3 MIAGE. Fablab UPSaclay. Gif-sur-Yvette, France. |
| Seminar. Licence 2 Droit, Sciences et Innovation. 3D Printing Principles. Fablab UPSaclay. Gif-sur-Yvette, France. |
| Work (continuous position/10th year). Director and Research engineer/ project Manager. Fablab UPSaclay + Université Paris Saclay. Gif-sur- Yvette, France. |

&nbsp;
&nbsp;
&nbsp;
&nbsp;

| `2022 (France)` |
| - |
| Mentoring. Master M2R - Design Research (ENS Paris-Saclay). Fablab UPSaclay. Gif-sur-Yvette, France. |
| Mentoring/Teaching. Master ARRC - Arts-Sciences-Technologies-social sciences (ENS Paris-Saclay). Fablab UPSaclay. Gif-sur-Yvette,|
| Mentoring. EIT-UPSaclay Master Human Computer Interaction (M1 and M2). Fablab UPSaclay. Gif-sur-Yvette, France. |
| Mentoring. "TP Innovant FAUCON" (Master Level) with Graduate School of Chemistry and Polytech. Fablab UPSaclay. Gif-sur-Yvette, France. |
Teaching. LICENCE 3 MIAGE. Fablab UPSaclay. Gif-sur-Yvette, France. |
| Seminar. Licence 2 Droit, Sciences et Innovation. 3D Printing Principles. Fablab UPSaclay. Gif-sur-Yvette, France. |
| Seminar. "Systemic Design for the Post-Oil Era". EUGLOH Innovation Days. Fablab UPSaclay. Gif-sur-Yvette, France. |
| Seminar. "Introduction to 3D Printing". EUGLOH. Fablab UPSaclay. Gif-sur-Yvette, France. |
| Work (continuous position/9th year). Director and Research engineer/ project Manager. Fablab UPSaclay + Université Paris Saclay. Gif-sur- Yvette, France. |

&nbsp;
&nbsp;
&nbsp;
&nbsp;

| `2021 (France)` |
| - |
| Participation. International Biolab Community Conference (remote). Global BioSummit. MIT, Boston, USA. |
| Lead on obtaining a 87k grant from the IDF Région (AAP Tiers-Lieux). |
| Seminar. Licence 2 Droit, Sciences et Innovation. 3D Printing Principles. Fablab UPSaclay. Gif-sur-Yvette, France. |
| Mentoring. Creartathon. INRIA + UPSaclay. Gif-sur-Yvette, France. |
| Mentoring. Fabricademy 2018 Instructor (distributed education program in fashion design and digital fabrication). September 2017 to March 2018. Fablab UPSaclay. Gif-sur-Yvette, France. |
| Mentoring. Fab Academy 2018 Instructor (distributed education program in digital fabrication). January 2016 to July 2016. Pr Neil Gershenfeld (CBA/MIT). Fablab UPSaclay. | Gif-sur-Yvette, France. |
| Talk. Textile et Numérique. Carrefour Numérique, Cité des Sciences et de l'Industrie, Paris, France |
| Work (continuous position/8th year). Director and Research engineer/ project manager. Fablab UPSaclay + Université Paris Saclay. Gif-sur- Yvette, France. |

&nbsp;
&nbsp;
&nbsp;
&nbsp;

| 2020 (France)
| - |
| International Biolab Community Conference (remote). Global BioSummit. MIT, Boston, USA. |
| Lead on obtaining a 100k grant from the IDF Préfécture. |
| Lead in digital fabrication and community management for UPSaclay’s COVID19 Initiative (fabrication of Protective Equipements) |
| Mentoring. Fabricademy 2018 Instructor (distributed education program in fashion design and digital fabrication). September 2017 to March 2018. Fablab UPSaclay. Gif-sur-Yvette, France. |
| Mentoring. Fab Academy 2018 Instructor (distributed education program in digital fabrication). January 2016 to July 2016. Pr Neil Gershenfeld (CBA/MIT). Fablab UPSaclay. Gif-sur-Yvette, France. |
| Seminar (remote). Co-chairing the Fablab Chamber at the OSI GENEVA FORUM. UN Headquarter, Geneva, Switzerland. |
| Work (continuous position/7th year). Director and Research engineer/ project manager. Fablab UPSaclay + Université Paris Saclay. Gif-sur- Yvette, France. |

&nbsp;
&nbsp;
&nbsp;
&nbsp;

| 2019 (France) |
| - |
| Participation. International Biolab Community Conference. Global BioSummit. MIT, Boston, USA. |
| Participation. International Fablab Community Conference. FAB15. Cairo, Egypt.
| Teaching. Fabricademy 2019 Instructor (distributed education program in fashion design and digital fabrication). Fablab UPSaclay. Gif-sur-Yvette, France. |
| Teaching. Fab Academy 2018 Instructor (distributed education program in digital fabrication). Pr Neil Gershenfeld (CBA/MIT). Fablab UPSaclay. Gif-sur-Yvette, France. |
| Seminar. Co-chairing the Fablab Chamber at the OSI GENEVA FORUM. UN Headquarter, Geneva, Switzerland.
| Work (continuous position/6th year). Director and Research engineer/ fablab Manager. Fablab UPSaclay + Université Paris Saclay. Gif-sur- Yvette, France. |

&nbsp;
&nbsp;
&nbsp;
&nbsp;

| `2018 (France)` |
| - |
| International Biolab Community Conference. Global BioSummit. MIT, Boston, USA. |
| Global Coordination of FAB14, the International Fablab Community Conference. UPSaclay + Toulouse (France). +1200 attendees. |
| Local Coordination of FAB14 Distributed "Science Research", the distributed version introducing the International Fablab Community Conference. UPSaclay (France). x10 guest-speakers + 100 attendees. |
| Teaching. Fabricademy 2018 Instructor (distributed education program in fashion design and digital fabrication). September 2017 to March 2018. Fablab UPSaclay. Gif-sur-Yvette, France. |
|Teaching. Fab Academy 2018 Instructor (distributed education program in digital fabrication). January 2016 to July 2016. Pr Neil Gershenfeld (CBA/MIT). Fablab UPSaclay. Gif-sur-Yvette, France. |
| Work (continuous position/5th year). Director and Research engineer/ fablab Manager. EX SITU team. (INRIA) + Fablab UPSaclay + Université Paris Saclay. Gif-sur-Yvette, France. |

&nbsp;
&nbsp;
&nbsp;
&nbsp;

| `2017 (France)` |
| - |
| Teaching. Fabricademy Node. Fablab UPSaclay. Gif-sur-Yvette, France.|
| International Fablab Community Support. Berytech/Agrytech (Incubator), Beirut, Lebanon. |
| International Biolab Community Conference. Global PBioSummit. MIT, Boston, USA. |
| International Fablab Community Conference. FAB13. Santiago de Chile, Chili. |
| Talks. Fablab, Usine Personnelle, Fab Factory, Impression 3D. JDEV. CNRS, France. |
| Workshop. How to start a Fablab. Colloque Pédagogie, Neurosciences et Numérique. Cachan, France. |
| Teaching. Fab Academy 2017 Instructor (distributed education program in digital fabrication). January 2016 to July 2016. Pr Neil Gershenfeld (CBA/MIT). Fablab UPSaclay. Gif-sur-Yvette, France. |
| Work (continuous position/4th year). Director and Research engineer/fablab Manager. AVIZ team. (INRIA) + Fablab UPSaclay + Université Paris Saclay. Gif-sur-Yvette, France.

&nbsp;
&nbsp;
&nbsp;
&nbsp;

| `2016 (France)` |
| - |
| Art. Building a mammalian cells incubator. Anne Stenne, Pierre Huyghe Studio. Palais de Tokyo, Paris, France. |
| Teaching. Bio Academy 2016 Instructor (distributed education program in synthetic biology). August 2016 to December 2016. Pr Georges Church (Harvard). Fablab UPSaclay. Gif-sur-Yvette, France. |
| Workshop. Documentation Formats and Technics Towards Fablab 2.0. Fab12. Sherraton Hotel, Shenzhen, China. |
| International Fablab Community Conference. FAB12. Shenzhen, China. |
| Jury. Fab Academy Global Evaluation Reviewer. Luciana Asinari. Fab Foundation. |
| Talk. Fablab et Recherche. Ecole Nationale Supérieure de la Photographie. Yannick Vernet. Mai 2016. Arles, France. |
| Study. Software Carpentry. Loic Esteve. Proto204. Bures-sur-yvette, France. |
| Talk. Qu'est-ce qu'un fablab ? Ecole Interdisciplinaire Université Paris-Saclay (EIDI). Du 04 au 08 avril 2016. Verrières le Buisson, France. |
| Training. February 2016. Ultimaker B.V. Head Quarter. Geldermalsen, The Netherlands. |
| Study. Avancées en biologie des systèmes et de synthèse. Francois Képès. Evry, France.
| Teaching. Fab Academy 2016 Instructor (distributed education program in digital fabrication). January 2016 to July 2016. Pr Neil Gershenfeld (CBA/MIT). Fablab UPSaclay. Gif-sur-Yvette, France. |
| Work (continuous position/3rd year). Research engineer/fablab Manager. AVIZ team. (INRIA) + Fablab UPSaclay + Université Paris Saclay. Gif-sur-Yvette, France.

&nbsp;
&nbsp;
&nbsp;
&nbsp;

| `2015 (France)` |
| - |
| Teaching. Digital Fabrication (8 weeks). HCID Master. With Michel Beaudouin-Lafon and Joseph Malloch. Fablab UPSaclay/Université-Paris-Sud. Gif-sur-Yvette, France.|
| Study. How To Grow Almost Anything (distributed education program in synthetic biology). August 2015 to December 2015. Pr Georges Church (Harvard). Fablab UPSaclay. Gif-sur-Yvette, France. |
| Event organizer. Richard Stallman. Conference on Free Software and Free Hardware Design. Digiteo Labs. Supélec. Gif-sur-Yvette, France. |
| Talk. How to create a fablab ? Nizar Kerkeni/CLIBRE. 220 peoples. Institut Supérieur des Etudes Techniques de Sousse. Sousse, Tunisia. |
| Workshop. Projects Documentation Formatting. Fab11. Massachusetts Institute of Technology (MIT). Cambridge MA, USA. |
| International Fablab Community Conference. FAB11, MIT, Boston, USA. |
| Study. Shopbot Tools training. Sallye Coyle. Shopbot Headquarter. Durham NC, USA. |
| Work. Local Community support. 3D Printshow2015. Fablab UPSaclay and Ultimaker B.V. Palacio de las Cibeles. Madrid, Spain. |
| Jury. Fablab projects evaluation. Master Innovation Technologique & Entrepreneuriat. Ecole Polytechnique. Sihem Jouini/Bruno Martinaud. Orsay, France. |
| Teaching. Open Innovation Week Lebanon. World Bank + AltCity + Lebanese Ministry of Telecommunication. Beirut, Lebanon. |
| Study. Arduino Tour Paris. With Massimo Banzi. ENSCI and le Fabshop. Paris, France. |
| Publication*. Bertin diy matrix Design. With Charles Perin and Mathieu Le Goc. Fablab UPSaclay. Gif- sur-Yvette, France. |
| Work (continuous position/2nd year). Research engineer/fablab Manager. AVIZ team. (INRIA) + Fablab UPSaclay + Université Paris Sud. Gif-sur-Yvette, France. |

&nbsp;
&nbsp;
&nbsp;
&nbsp;

| `2014 (France)` |
| - |
| Art Exhibition. Audio editing. Colette. With L.Alexis. Radio22 Tout-Monde. Le QG du 116/Espace Khiasma. Montreuil, France. |
| Work. Local community Support. 3D Printshow2014. Fablab UPSaclay/Ultimaker B.V. Carrousel du Louvre. Paris, France. |
| Teaching. Digital Fabrication and Design Thinking (8 weeks). HCID Master. With L.Oehlberg. Fablab UPSaclay, Université-Paris-Sud. Gif-sur-Yvette, France. |
| Work. Local community Support. Maker Faire Rome. For Fablab UPSaclay/Ultimaker B.V. Parco della Musica. Rome, Italy. |
| Work. Local community Support. For Fablab UPSaclay/Ultimaker B.V. FAB10, DissenyHub. Barcelona, Spain. |
| International Fablab Community Conference. FAB10, IAAC ,Barcelona, Spain. |
| Talk. Lift the FING. Proto204. With la FING. Bures-sur-Yvette, France. |
| Publication*. Digital fabrication of open-source Froebel Gift. S.Huron/M.Le Goc/J.Boy/JD.Fekete. Gaité Lyrique. Paris, France. |
| Talk. Made with Makers. Futur En Seine. Proto204. Bures-sur-Yvette, France. |
| Art Exhibition. Audio recording + editing. La voix est libre. Radio22 Tout-Monde. Le QG du 116/Espace Khiasma. Montreuil, France. |
| Work. Local community support. With MakerShop for Ultimaker B.V. Maker Faire Paris. Le 104. Paris France. |
| Jury. Appel à projet Art-Sciences International. Diagonale Paris-Saclay. Gif-sur-Yvette, France. |
| Art Exhibition. Collective public performance (vocal). Sartori. A.Recalde Miranda. Le QG du 116, Montreuil, France. |
| Work (continuous position/1st year). Research engineer/fablab Manager. AVIZ team. (INRIA) + Fablab UPSaclay + Université Paris Saclay. Gif-sur-Yvette, France. |

&nbsp;
&nbsp;
&nbsp;
&nbsp;

| `2013 (France)` |
| - |
| Work. Local Community support. 3D Printshow2013. Ultimaker. Carrousel du Louvre. Paris, France. |
| Work. Video making + show managment. Master SPEAP. Bruno Latour + Valérie Pihet. Sciences Po Paris, France. |
| Work. Consulting. Building a fablab. Quatorze (architects). Montreuil, France. |
| Artist Residency (group). Le QG (in process). Marlène Rigler. Le 116, Montreuil, France. |
| Work. Video making + video editing. Jean-Pierre Seyvos. TAP, Poitiers, France. |
| Work. Video making + video editing. Bruno Latour. AIME. Medialab Sciences Po Paris. |
| Work. Video making + video editing. Dominique Boullier. FORCCAST. Medialab Sciences Po Paris. |
| Work. 3D printing demo. Le Fabshop. Espace P.Cardin/Cetelem. Paris, France. |
| Publication (collective). Mouvement n°70. The Bells Angels/speap. Journal Clandestin. Paris, France. |

&nbsp;
&nbsp;
&nbsp;
&nbsp;

| `2012 (USA + France)` |
| - |
| Studies. Experimental Master in Arts and Politics SPEAP. Sciences Po Paris, France. |
| Work. 1 mission as a fundraising team manager for Greenpeace. France. |
| Studies. FAB ACADEMY DIPLOMA. Pr Neil Gershenfeld (MIT). Providence, RI, USA (6 months). |
| Artist Residency (solo). AS220. Providence RI, USA (7 months). |

&nbsp;
&nbsp;
&nbsp;
&nbsp;

| `2011 (France)` |
| - |
| Work. 5 mission as a fundraising team manager for Greenpeace. France (7 months). |
| Art Exhibition. SCINEMA 2011 (Australian Science Week / 120 projections). Australia. |
| Art Exhibition. Museum of Pocket Art. curated by Grand Detour. Portland, USA at Champion Art Gallery. Austin, USA. |
| Art Exhibition. LOOP ART FAIR. Curated by Patricia Ciriani. Barcelona, Spain. |
| Art Projection. MIT 7th European Short Film Festival. Cambridge, USA. |


&nbsp;
&nbsp;
&nbsp;
&nbsp;

| 2010 (France) |
| - |
| Art Exhibition. Instructions for Initial Conditions. Drift Station Gallery. Lincoln, USA. |
| Art Exhibition. Show Room Docks Design. Bordeaux, France. |
| Art Exhibition. Krack Project (monumental public art installation). Selection for Art & Biodiversité >, Paris, France. Collaboration with la World Wide Foundation (WWF) and C.O.A.L. |
| Publication. Le Katalog de l’Intelligence Humaine n°3. Paris-London-Bordeaux-Istambul. |
| Art Exhibition. Selection for Art Bin. South London Gallery. London, Angleterre.
2009 (France) |
| Art Exhibition. Sculpture en Ville. (Krack Project, monumental public art installation), Bordeaux, France. |

&nbsp;
&nbsp;
&nbsp;
&nbsp;

| `2008 (France)` |
| - |
| Grant. DRAC Aquitaine. Bordeaux, France. |
| Art Exhibition. Lieux Possibles. curated by Bruit du Frigo, Bordeaux, France. |
| Art Exhibition. Causes/Consequences (solo show). Librairie Mollat, Bordeaux, France. |

&nbsp;
&nbsp;
&nbsp;
&nbsp;

| `2004-2005-2006-2007 (France + Argentina)` |
| - |
| Art Exhibition. Pasto (solo exhibition), Alliance française. Buenos Aires, Argentina. |
| Art Exhibition. Patagonia Otra (Seminar, Workshop, public intervention, drawings exhibition), collaboration with Grupo CRTrw (architects). Universidad de Trelew/Patagonia, Argentina. |
| Work. Permanent Position at Greenpeace France. 12 mission as a fundraising team manager for Greenpeace. France. |

&nbsp;
&nbsp;
&nbsp;
&nbsp;

| `2002-2003 (Spain)` |
| - |
| Art Exhibition. Paintings (solo show), Taller Galeria. Barcelona, Spain.
1999 – 2000 (France) |
| Studies. English + Art History. Université Bordeaux III. |

&nbsp;
&nbsp;
&nbsp;
&nbsp;

| `1994 - 1998 (France)` |
| - |
| Studies. Baccalauréat Visual Arts and Art History. Évreux, France. |

&nbsp;
&nbsp;
&nbsp;
&nbsp;

| `1993 (France)` |
| - |
| First art paintings |

&nbsp;
&nbsp;
&nbsp;
&nbsp;

| `1978 (France)` |
| - |
| Birth in Aubervilliers, France. |

&nbsp;
&nbsp;
&nbsp;
&nbsp;


## `DETAILED LIST OF SEMINAR AND COURSES I TOOK :`

| List of Seminars from the Experimental Master in Arts and Politics (SPEAP) at Sciences Po Paris by Bruno Latour (and more): |
| - |
|SPEAP's Supervisory Committee was composed of : Bruno Latour, Valérie Pihet, Émilie Hermant, Sébastien Thiéry, Jean-Michel Frodon, Antoine Hennion. |
| 1 - "Histoire du Pragmatisme" par Bruno Latour, sociologue, anthropologue, philosophe des sciences directeur scientifique de Sciences Po. |
| 2 - "Qu'est-ce qu'un public ?" par Bruno Latour. |
| 3 - "Épistémologie française" par Antoine Hennion, Sociologue, professeur des Mines Paris- Tech et Centre de l'Innovation Sociologique. |
| 4 - "Éthnométhodologie, les travaux de Goodwin et Gibson", par Franck Leibovici, artiste contemporain. |
| 5 - "Cosmologies" : Le travail des ONG sur le terrain par Amita Baviskar, sociologue et anthropologue, Institute of Economic Growth, Delhi, Inde. |
| 6 - "Cosmologies" : The Power of Nuclear Things, par Gabrielle Hecht, Docteur en Histoire et Sociologie des Sciences, Université du Michigan, USA. |
| 7 - "Cosmologies" : Computer Models, climate data and the Politics of Global Warming par Paul Edwards, Docteur en Histoire de la Conscience, Université du Michigan, USA. |
| 8 - "Analyse conversationnelle" par Lorenza Mondada, Docteur en linguistique, Université de Bale, Suisse. |
| 9 - "Whitehead et le concept de Bifurcation de la Nature", Didier Debaise, philosophe, Université Libre de Bruxelles. |
| 10 - "Les Dispositifs d'écriture des poètes et de artistes", par Franck Leibovici artiste contemporain. |
| 11 - "Socio-Sémiotique" par Bruno Latour. |
| 12 - "Restitution d'une méthode de reportage" par Stefano Savona, cinéaste. |
| 13 - "La statistique en sciences humaines", Medialab de Sciences Po par Paul Girard, ingénieur généraliste des technologies numériques. |
| 14 - "Géographie de l'information avec le logiciel Gephi", Medialab de Sciences Po par Mathieu Jacomy, ingénieur, cartographe du web. |
| 15 - "Le concept de Bifurcation de la Nature" par Simon Schaffer, historien et philosophe des sciences, Cambridge, Royaume-Uni. |
| 16 - "Enquête sur les Modes d'Existences" par Bruno Latour. |
| 17 - "Essai de classification des positions autours des rapports entre humanité et cosmos", par Eduardo Viveiros de Castro, anthropologue, CNRS, Cambridge. |
| 18 - "Exercice de présentation d'un animal" par Vinciane Despret, philosophe des sciences et ethnologue, université de Liège. |
| 19 - "Les animaux au travail" par Jocelyne Porcher, chercheuse à l'INRA. |
| 20 - "La ballade des yeux" par Myriam Leftkowitz, historienne et danseuse contemporaine studio Trisha Brown, studio Cunningham. |
| 21 - "Récit autobiographique" par Patrick Bouchain, architecte, scénographe, politique.
| 22 - Discussion sur le film "Les Eclats" par Sylvain Georges, Cinéaste. |
| 23 - Sur "l'Histoire de l'Art" par Antoine Hennion, Sociologue, professeur des Mines Paris- Tech et Centre de l'Innovation Sociologique. |
| 24 - Sur "l'ambiguïté visuelle dans le travail de Gauguin" par Dario Gamboni, historien de l'art, Université de Genève, Suisse. |
| 25 - "Introduction a l'Éthno-psychiatrie" par Tobi Nathan, professeur émérite de psychologie clinique et pathologique, éthno-psychiatre, diplomate, Centre Georges Deveureux, Paris. |
| 26 - "La maltraitante théorique". Francoise Sironi, ethno-psychologue et psychothérapeute à La Cours Pénale Internationale. |
| 27 - Sur le film "J'ai mangé tous mes enfants" par Emmanuelle Ohniguian, éthnopsychiatre et psychothérapeute. |
| 28 - Sur "les formes démocratiques de la participation". Joëlle Zask, philosophe du pragmatisme, traductrice de Dewey, Université de Provence. |
| 29 - "Théorie et exercice Rituel". Michael Houseman, Centre d'Étude des Mondes Africains, CNRS, École Pratique des Hautes Études, Paris. |
| 30 - Collective publication with REVUE MOUVEMENT and the Bells Angels (2D designers). |
| 31 - "Sur la Maitrise d'oeuvre" par Patrick Bouchain, architecte, ancien conseiller de Jacques Lang au Ministère de la Culture. |

&nbsp;
&nbsp;
&nbsp;
&nbsp;

| `Details on the Fab Academy Course - By Neil Gershenfeld (CBA/MIT) and Anna Kaziunas-France (january 2012 to june 2012) :` |
| - |
| week_1_fab_academy_principles_and_practices. |
| week_2_fab_academy_project_management. |
| week_3_fab_academy_computer_aided_design. |
| week_4_fab_academy_computer_controlled_cutting. |
| week_5_fab_academy_electronics_production. |
| week_6_fab_academy_computer_controlled_machining. |
| week_7_fab_academy_electronics_design. |
| week_8_fab_academy_molding_and_casting_composites. |
| week_9_fab_academy_embedded_programming. |
| week_10_fab_academy_3d_scanning_and_printing. |
| week_11_fab_academy_input_devices. |
| week_12_fab_academy_interface_and_application_programming. |
| week_13_fab_academy_output_devices. |
| week_14_fab_academy_mechanical_design. |
| week_15_fab_academy_networking_and_communication. |
| week_16_fab_academy_machine_design. |
| week_17_fab_academy_applications_and_implications. |
| week_18_fab_academy_invention_intellectual_property_and_business_models. |
| week_19_fab_academy_project_development. |
| week_20_fab_academy_final_project_presentation. |

&nbsp;
&nbsp;
&nbsp;
&nbsp;

| `Details on the Bio Academy Course - By George Church (Wyss Institute/Harvard) and David Kong (august 2015 to january 2016) :` |
| - |
| week_1_bio_academy_Principles and Practices - 26 August |
| week_2_bio_academy_DNA Nanostructures - 2 September |
| week_3_bio_academy_Synthetic Minimal Cells - 9 September |
| week_4_bio_academy_Next generation synthesis - 16 September |
| week_5_bio_academy_Bio production - 23 September |
| week_6_bio_academy_Darwin on steroids: Bio design, diversity and selection - 30 September |
| week_7_bio_academy_Genome Engineering - 7 October |
| week_8_bio_academy_Fluorescence In Situ Sequencing (FISSEQ) - 14 october |
| week_9_bio_academy_Synthetic development biology - 21 october |
| week_10_bio_academy_Biofabrication and additive manufacturing - 28 october |
| week_11_bio_academy_Gene Drives and Synthetic Ecosystems - 4 november |
| week_12_bio_academy_Engineering the Human Gut Microbiome - 11 November |
| week_13_bio_academy_Computational protein design, biosensors and the folding game |
| week_14_bio_academy_Tool Chains, Automation, and Open Hardware - 2 December |
| week_15_bio_academy_Invention, intellectual property - 9 December |
| week_16_bio_academy_Final presentations - 13 January |

&nbsp;
&nbsp;
&nbsp;
&nbsp;

| `Details on the Fabricademy Course - By Anastasia Pistofidou (IAAC) and Cecilia Raspanti (The WAAG Society) (september 2018 to march 2019) :` |
| - |
| week_1_Fabricademy_STATE OF THE ART, PROJECT MANAGEMENT AND DOCUMENTATION - september 25 |
| week_2_Fabricademy_DIGITAL BODIES - october 2 - by Anastasia Pistofidou |
| week_3_Fabricademy_CIRCULAR OPEN SOURCE FASHION - october 9 - by Zoe Romano |
| week_4_Fabricademy_BIOFABRICATING DYES AND MATERIALS - october 16 - by Cecilia Raspanti |
| week_5_Fabricademy_E-TEXTILES - october 23 - Liza Stark |
| week_6_Fabricademy_TEXTILE AS SCAFFOLD - october 30 - by Anastasia Pistofidou |
| week_7_Fabricademy_COMPUTATIONAL COUTURE - november 6 - by Aldo Solazzo |
| week_8_Fabricademy_OPEN SOURCE HARDWARE: FROM FIBERS TO FABRIC - november 13 - by Mar Canet & Varvara Guljajeva |
| week_9_Fabricademy_WEARABLES - november 20 - by Liza Stark |
| week_10_Fabricademy_IMPLICATIONS AND APPLICATIONS - november 27 - by Oscar Tomico |
| week_11_Fabricademy_SOFT ROBOTICS - december 4 - by Lily Chambers & Adriana Cabrera |
| week_12_Fabricademy_Engineering the Human Gut Microbiome - 11 November |
| week_13_Fabricademy_SKIN ELECTRONICS - december 11 - by Katia Vega |
| week_14_Fabricademy_PROJECT PROPOSAL PRESENTATION - December 18 |
